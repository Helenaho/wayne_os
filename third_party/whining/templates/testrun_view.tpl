%# Copyright (c) 2012 The Chromium OS Authors. All rights reserved.
%# Use of this source code is governed by a BSD-style license that can be
%# found in the LICENSE file.

%from src import settings
%_root = settings.settings.relative_root


%def time_with_tooltip(utctime):
  %if utctime == None:
    <div> </div>
    %return
  %end
  %zulu_time = utctime.isoformat() + 'Z'
  %display_time = utctime.strftime('%Y-%m-%d %H:%M:%S (UTC)')
  <div onmouseover="this.title=(new Date('{{ zulu_time }}'));" title="local?">
    {{ display_time }}
  </div>
%end


%def body_block():
  %# ------------------------------------------------------------------------
  %# Switcher toolbar allows query parameter removal.
  %include('switcher_bar.tpl', tpl_vars=tpl_vars, query_string=query_string, url_base='testruns')

  %# ------------------------------------------------------------------------
  %# Test runs
  %_test_runs = tpl_vars.get('test_runs')
  %_filter_tag = tpl_vars.get('filter_tag')
  %_statcls =  tpl_vars.get('status_classes')
  %_host =  tpl_vars.get('host')
  %_user =  tpl_vars.get('user')
  %if not _test_runs:
    <h3><u>No test runs found.</u></h3>
    %return
  %end

  %# ------------------------------------------------------------------------
  <div id="divTests" class="lefted">
    <table class="alternate_background">
      <tbody>
        %# ---- Test ID -----
        <tr>
          <th class="righted">Test ID</th>
          %for _tr in _test_runs:
            <td class="lefted">{{ _tr['test_idx'] }}</td>
          %end
        </tr>
        %# ---- Test name -----
        <tr>
          <th class="righted">Test name</th>
          %for _tr in _test_runs:
            <td class="lefted">{{ _tr['test_name'] }}</td>
          %end
        </tr>
        %# ---- Suite -----
        <tr>
          <th class="righted">Suite</th>
          %for _tr in _test_runs:
            <td class="lefted">{{ _tr['suite'] }}</td>
          %end
        </tr>
        %# ---- Platform -----
        <tr>
          <th class="righted">Platform</th>
          %for _tr in _test_runs:
            <td class="lefted">{{ _tr['platform'] }}</td>
          %end
        </tr>
        %# ---- Builder config -----
        <tr>
          <th class="righted">Builder config</th>
          %for _tr in _test_runs:
            <td class="lefted">{{ _tr['config'] }}</td>
          %end
        </tr>
        %# ---- Build -----
        <tr>
          <th class="righted">Build</th>
          %for _tr in _test_runs:
            <td class="lefted">{{ _tr['build'] }}</td>
          %end
        </tr>
        %# ---- Experimental? -----
        <tr>
          <th class="righted">Experimental</th>
          %for _tr in _test_runs:
            <td class="lefted">
              {{ 'Yes' if _tr['is_experimental'] else 'No' }}
            </td>
          %end
        </tr>
        %# ---- Failures view link -----
        <tr>
          <th class="righted">Other views</th>
          %for _tr in _test_runs:
            <td class="lefted">
              %_qsdict = {
              %  'suites': _tr['suite'],
              %  'tests': _tr['test_name'],
              %  'platforms': _tr['platform'],
              %  'builds': _tr['build'],
              %  'releases': _tr['release_number'],
              %  }
              %_qs = query_string(add=_qsdict, remove=['test_ids'])
              %if _tr['status'] != 'GOOD':
                <a target="_blank" href="{{ _root }}/failures/{{ _filter_tag }}{{_qs}}">
                  failures
                </a>,
              %end
              %_qsdict.pop('platforms')
              %_qsdict.pop('builds')
              %_qsdict['days_back'] = 14
              %_qs = query_string(add=_qsdict, remove=['test_ids'])
              <a target="_blank" href="{{ _root }}/matrix/{{ _filter_tag }}{{ _qs }}">
                  matrix
              </a>
            </td>
          %end
        </tr>
        %# ---- Status -----
        <tr>
          <th class="righted">Status</th>
          %for _tr in _test_runs:
            %_status = _tr['status']
            <td><div class="bordereditem {{ _statcls[_status] }}">
              {{ _status }}
            </div></td>
          %end
        </tr>
        %# ---- Reason -----
        <tr>
          <th class="righted">Reason</th>
          %for _tr in _test_runs:
            <td class="lefted">{{ _tr['reason'] }}</td>
          %end
        </tr>
        %# ---- Job name -----
        <tr>
          <th class="righted">Job name</th>
          %for _tr in _test_runs:
            <td class="lefted">{{ _tr['job_name'] }}</td>
          %end
        </tr>
        %# ---- Job queued time -----
        <tr>
          <th class="righted">Job queued time</th>
          %for _tr in _test_runs:
            <td class="lefted">
              %time_with_tooltip(_tr['job_queued_time'])
            </td>
          %end
        </tr>
        %# ---- Latency -----
        <tr>
          <th class="righted">Time in queue [s]</th>
          %for _tr in _test_runs:
            %if not _tr['test_started_time'] or not _tr['job_queued_time']:
                %continue
            %end
            %delta = _tr['test_started_time'] - _tr['job_queued_time']
            <td class="lefted">{{ '%0.0f' % delta.total_seconds() }}</td>
          %end
        </tr>

        %# ---- Start time -----
        <tr>
          <th class="righted">Test start time</th>
          %for _tr in _test_runs:
            <td class="lefted">
              %time_with_tooltip(_tr['test_started_time'])
            </td>
          %end
        </tr>
        %# ---- End time -----
        <tr>
          <th class="righted">Test end time</th>
          %for _tr in _test_runs:
            <td class="lefted">
              %time_with_tooltip(_tr['test_finished_time'])
            </td>
          %end
        </tr>
        %# ---- Duration -----
        <tr>
          <th class="righted">Duration [s]</th>
          %for _tr in _test_runs:
            %if not _tr['test_finished_time'] or not _tr['test_started_time']:
                %continue
            %end
            %delta = _tr['test_finished_time']- _tr['test_started_time']
            <td class="lefted">{{ '%0.0f' % delta.total_seconds() }}</td>
          %end
        </tr>

        %# ---- Hostname -----
        <tr>
          <th class="righted">DUT host name</th>
          %for _tr in _test_runs:
            <td class="lefted">
            <a target="_blank"
              href="{{ _host }}/afe/#tab_id=view_host&hostname={{ _tr['hostname'] }}">
              {{ _tr['hostname'] }}
            </a>
            </td>
          %end
        </tr>
        %# ---- Cautotest -----
        <tr>
          <th class="righted">cautotest</th>
          %for _tr in _test_runs:
            <td class="lefted">
              <a target="_blank" href="{{ _host }}/afe/#tab_id=view_job&object_id={{ _tr['afe_job_id'] }}">
                Job page</a>,
              <a target="_blank" href="{{ _host }}/new_tko/#tab_id=test_detail_view&object_id={{ _tr['test_idx'] }}">
                Test page</a>
            </td>
          %end
        </tr>
        %# ---- Logs -----
        <tr>
          <th class="righted">Logs</th>
          %_log_url = '%s/tko/retrieve_logs.cgi?job=/results/' % _host
          %for _tr in _test_runs:
            <td class="lefted">
              <a target="_blank"
                href="{{ _log_url }}{{ _tr['afe_job_id'] }}-{{ _user }}">
                Job logs</a>,
              <a target="_blank"
                href="{{ _log_url }}{{ _tr['afe_job_id'] }}-{{ _user }}/{{_tr['hostname']}}/{{ _tr['test_name'] }}">
                Test logs</a>
            </td>
          %end
        </tr>
        %# ---- Sources -----
        <tr>
          <th class="righted">Sources</th>
          %for _tr in _test_runs:
            <td class="lefted">
              %_test_name = _tr['test_name'].split('.')[0]
              %_cs_qry = ' '.join([
              %  _test_name,
              %  'package:^chromeos_public$',
              %  'file:^src/third_party/autotest/files/',
              %  ])
              <!--- TODO (drinkcat): Make this configurable -->
              <a target="_blank"
                href="https://cs.corp.google.com/#search/&q={{ _cs_qry }}">
                Code search</a>,
              %# Gitiles has no built in search, making links for both client
              %# and server subdirs, one of them should work.
              % _url = ('https://chromium.googlesource.com'
                               %'/chromiumos/third_party/autotest/+/master/'
                               %'%s/site_tests/%s')
              <span
                title="If unsure, try both links, one of them should work.">
                Gitiles:
              </span>
              <a target="_blank"
                href="{{ _url % ('client', _test_name) }}">
                client
              </a>
              /
              <a target="_blank"
                href="{{ _url % ('server', _test_name) }}">
                server
              </a>

            </td>
          %end
        </tr>
        %# ---- Bugs -----
        <tr>
          <th class="righted">Tracker</th>
          %for _tr in _test_runs:
            <td class="lefted">
                <a target="_blank"
                  href="//code.google.com/p/chromium/issues/list?q={{ _tr['test_name']}}">
                  Search bugs
                </a>
            </td>
          %end
        </tr>
        %# ---- AutoBugs -----
        <tr>
          <th class="righted">Auto Bug</th>
          %for _tr in _test_runs:
            %_bug_id = _tr['bug_id']
            <td class="lefted">
              %if _bug_id is not None:
                <a target="_blank"
                  href="http://crbug.com/{{ _bug_id }}">
                  crbug.com/{{ _bug_id }}
                </a>
              %end
            </td>
          %end
        </tr>
        %# ---- Retry of -----
        <tr>
          <th class="righted">Retry of</th>
          %for _tr in _test_runs:
            %_retry_of = _tr['retry_of']
            <td class="lefted">
              %if _retry_of is not None:
                <a target="_blank"
                  href="/testrun/?test_ids={{ _retry_of }}">
                  {{ _retry_of }}
                </a>
              %end
            </td>
          %end
        </tr>
        %# ---- Retried by -----
        <tr>
          <th class="righted">Retried by</th>
          %for _tr in _test_runs:
            %_retried_by = _tr['retried_by']
            <td class="lefted">
              %if _retried_by is not None:
                <a target="_blank"
                  href="{{ _root }}/testrun/?test_ids={{ _retried_by }}">
                  {{ _retried_by }}
                </a>
              %end
            </td>
          %end
        </tr>

      </tbody>
    </table>
  </div>
%end

%rebase('master.tpl', title='testruns', body_block=body_block, query_string=query_string)
