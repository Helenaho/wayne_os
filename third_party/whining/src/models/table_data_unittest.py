#!/usr/bin/python
# Copyright (c) 2012 The Chromium OS Authors. All rights reserved.
# Use of this source code is governed by a BSD-style license that can be
# found in the LICENSE file.

"""Unit tests for table_data.py data structures."""

import datetime
import time
import unittest

import table_data


def _make_tag(row_header, col_header):
    """Helper to make row col tag for checking."""
    return '%s %s' % (row_header, col_header)


class CellDataTest(unittest.TestCase):
    """Unit Tests for class CellData."""

    def setUp(self):
        """Populate test data that is used in multiple tests."""
        self._data = ['GOOD'] * 9 + ['ERROR'] * 4

        self._cell = table_data.CellData()
        for d in self._data:
            self._cell.add(d)

    def test_status_collection(self):
        self.assertIn('GOOD', self._cell.data)
        self.assertIn('ERROR', self._cell.data)
        self.assertNotIn('ABORT', self._cell.data)
        self.assertEqual(self._cell.data['GOOD'], 9)
        self.assertEqual(self._cell.data['ERROR'], 4)
        self.assertEqual(self._cell.count, 13)


class CustomHeaderCollectorTest(unittest.TestCase):
    """Unit Tests for class CustomHeaderCollector."""

    def test_collect_first(self):
        """Handle cases where the same value repeated by grabbing first."""
        chc = table_data.CustomHeaderCollector()
        dummy_release = 'R24-1234.0.0'
        for i in xrange(1, 100):
            chc.add(dummy_release, i)
        self.assertEqual(chc.data, {dummy_release: 1})

    def test_collect_min_int(self):
        """Preserve the smallest value."""
        chc = table_data.CustomHeaderCollector_Min()
        dummy_release = 'R24-1234.0.0'
        for i in xrange(100, 1, -1):
            chc.add(dummy_release, i)
        self.assertEqual(chc.data, {dummy_release: 2})

    def test_collect_min_datetime(self):
        """Preserve the smallest value (e.g. earliest time)."""
        dt1 = datetime.datetime.now()
        time.sleep(0.5)
        dt2 = datetime.datetime.now()
        time.sleep(0.5)
        dt3 = datetime.datetime.now()
        chc = table_data.CustomHeaderCollector_Min()
        dummy_release = 'R24-1234.0.0'
        chc.add(dummy_release, dt3)
        chc.add(dummy_release, dt1)
        chc.add(dummy_release, dt2)
        self.assertEqual(chc.data, {dummy_release: dt1})
        self.assertNotEqual(chc.data, {dummy_release: dt2})
        self.assertNotEqual(chc.data, {dummy_release: dt3})


def _split_build(full_build):
    """Helper to split the full build into useful sub-parts.

    Args:
        full_build: full build is expected to be in a format as follows:
                      Rww-xx.yy.zz
                    The ww (numeric) portion usually maps directly to the
                    Chrome version (so we use it as such in tests).

    Returns:
        Tuple of (release, chrome_build) both used in test verification.
        For example: R24-1234.00.00 would return (R24, 24.1234.0.0).
    """
    build_split = full_build.split('-')
    release = build_split[0]
    chrome_release = int(release[1:])
    short_build = build_split[1]
    chrome_build = '%s.%s' % (chrome_release, short_build)
    return release, chrome_build


class ViewTableDataTest(unittest.TestCase):
    """Unit Tests for class ViewTableData and SortedBuildTableData."""

    def setUp(self):
        """Populate test data that is used in multiple tests."""
        self._unsorted_platforms = [
            'mario', 'lumpy', 'snow', 'alex', 'link']
        self._sorted_platforms = [
            'alex', 'link', 'lumpy', 'mario', 'snow']

        self._unsorted_builds = [
            'R23-1000', 'R23-1005', 'R23-1004', 'R23-1010', 'R23-1001',
            'R25-1787', 'R25-1700', 'R25-1744', 'R25-1799', 'R25-1675',
            'R24-1333', 'R24-1301', 'R24-1402', 'R24-1388', 'R24-1322']
        self._unsorted_builds = [
            '%s.0.0' % b for b in self._unsorted_builds]
        self._improperly_sorted_builds = sorted(self._unsorted_builds)
        self._sorted_builds = [
            'R25-1799', 'R25-1787', 'R25-1744', 'R25-1700', 'R25-1675',
            'R24-1402', 'R24-1388', 'R24-1333', 'R24-1322', 'R24-1301',
            'R23-1010', 'R23-1005', 'R23-1004', 'R23-1001', 'R23-1000',]
        self._sorted_builds = [
            '%s.0.0' % b for b in self._sorted_builds]

        self._unsorted_test_names = [
            'sleeptest', 'platform_Shutdown', 'platform_BootPerf',
            'power_LoadTest']
        self._sorted_test_names = [
            'platform_BootPerf', 'platform_Shutdown', 'power_LoadTest',
            'sleeptest']

        self._unsorted_statuses = ['GOOD', 'FAIL', 'ERROR', 'ABORT']

        # Add all the rows to simulate the rows in the db each
        # with: release, platform, test_name, status
        self._test_data = []
        for b in self._unsorted_builds:
            for p in self._unsorted_platforms:
                for t in self._unsorted_test_names:
                    for s in self._unsorted_statuses:
                        self._test_data.append([b, p, t, s])

        # Populate a TableData() and SortedBuildTableData() with the rows.
        self._td = table_data.TableData('release', 'test name',
                                        'test results by release')
        self._sbtd = table_data.SortedBuildTableData('platform', 'test results '
                                                     'by build & platform')
        # Add some custom headers.
        self._chrome_ver = 'Chrome Ver'
        self._start_time = 'Start Time'
        self._sbtd.init_custom_col_header(
            self._chrome_ver, table_data.CustomHeaderCollector())
        self._sbtd.init_custom_col_header(
            self._start_time, table_data.CustomHeaderCollector_Min())

        self._releases = set()
        for build, platform, test_name, status in self._test_data:
            self.assertIn('-', build)
            release, chrome_build = _split_build(build)
            self._releases.add(release)
            self._td.add(release, test_name, status)
            # stuff tag into each cell for transpose testing
            self._td.add(release, test_name, _make_tag(release, test_name))

            self._sbtd.add(build, platform, status)
            # stuff tag into each cell for transpose testing
            self._sbtd.add(build, platform, _make_tag(build, platform))

            self._sbtd.add_custom_col_header(self._chrome_ver, build,
                                             chrome_build)
            self._sbtd.add_custom_col_header(self._start_time, build,
                                             datetime.datetime.now())

    def test_row_label_verify(self):
        self.assertEquals(self._td.row_label, 'release')
        self.assertEquals(self._sbtd.row_label, 'Build')

    def test_row_label_verify_transposed(self):
        self._td.transpose = True
        self.assertEquals(self._td.row_label, 'test name')
        self._td.transpose = False

        self._sbtd.transpose = True
        self.assertEquals(self._sbtd.row_label, 'platform')
        self._sbtd.transpose = False

    def test_col_label_verify(self):
        self.assertEquals(self._td.col_label, 'test name')
        self.assertEquals(self._sbtd.col_label, 'platform')

    def test_col_label_verify_transposed(self):
        self._td.transpose = True
        self.assertEquals(self._td.col_label, 'release')
        self._td.transpose = False

        self._sbtd.transpose = True
        self.assertEquals(self._sbtd.col_label, 'Build')
        self._sbtd.transpose = False

    def test_title_verify(self):
        self.assertEquals(self._td.title, 'test results by release')
        self.assertEquals(self._sbtd.title, 'test results by build & platform')

    def test_row_headers_sorted(self):
        self.assertEquals(self._td.row_headers, sorted(self._releases))
        self.assertEquals(self._sbtd.row_headers, self._sorted_builds)

    def test_row_headers_sorted_transposed(self):
        self._td.transpose = True
        self.assertEquals(self._td.row_headers, self._sorted_test_names)
        self._td.transpose = False

        self._sbtd.transpose = True
        self.assertEquals(self._sbtd.row_headers, self._sorted_platforms)
        self._sbtd.transpose = False

    def test_col_headers_sorted(self):
        self.assertEquals(self._td.col_headers, self._sorted_test_names)
        self.assertEquals(self._sbtd.col_headers, self._sorted_platforms)

    def test_col_headers_sorted_transposed(self):
        self._td.transpose = True
        self.assertEquals(self._td.col_headers, sorted(self._releases))
        self._td.transpose = False

        self._sbtd.transpose = True
        self.assertEquals(self._sbtd.col_headers, self._sorted_builds)
        self._sbtd.transpose = False

    def test_row_headers_unsorted(self):
        self._td.sort_rows = False
        self.assertNotEquals(self._td.row_headers, sorted(self._releases))
        self._td.sort_rows = True

        self._sbtd.sort_rows = False
        self.assertNotEquals(self._sbtd.row_headers, self._sorted_builds)
        self._sbtd.sort_rows = True

    def test_col_headers_unsorted(self):
        self._td.sort_cols = False
        self.assertNotEquals(self._td.col_headers, self._sorted_test_names)
        self._td.sort_cols = True

        self._sbtd.sort_cols = False
        self.assertNotEquals(self._sbtd.col_headers, self._sorted_platforms)
        self._sbtd.sort_cols = True

    def test_custom_col_headers(self):
        # custom col headers should be sorted.
        pass

    def test_custom_row_headers(self):
        # custom row headers should be transposed col headers.
        pass

    def test_get_cell(self):
        # Expected cell data counts:
        #   200 = 25x for each of 4 statuses + 100 tags.
        #   32 = 4x for each of 4 statuses + 16 tags.
        for td, cell_count in [(self._td, 200), (self._sbtd, 32)]:
            for r in td.row_headers:
                for c in td.col_headers:
                    cell = td.get_cell(r, c)
                    self.assertEquals(cell.count, cell_count)
                    self.assertIn(_make_tag(r, c), cell.data)
                    self.assertTrue(set(self._unsorted_statuses) <=
                                    set(cell.data.keys()))
    def test_get_cell_transposed(self):
        # Expected cell data counts:
        #   200 = 25x for each of 4 statuses + 100 tags.
        #   32 = 4x for each of 4 statuses + 16 tags.
        for td, cell_count in [(self._td, 200), (self._sbtd, 32)]:
            td.transpose = True
            for r in td.row_headers:
                for c in td.col_headers:
                    cell = td.get_cell(r, c)
                    self.assertEquals(cell.count, cell_count)
                    self.assertIn(_make_tag(c, r), cell.data)
                    self.assertTrue(set(self._unsorted_statuses) <=
                                    set(cell.data.keys()))
            td.transpose = False


if __name__ == "__main__":
    unittest.main()
