From b9b67f5ceb55a4b64203b7fdccf7421ffe77c3b4 Mon Sep 17 00:00:00 2001
From: Michael Martis <martis@chromium.org>
Date: Mon, 29 Apr 2019 17:27:14 +1000
Subject: [PATCH] Switched to library-provided http_archive.

The version of Bazel with which TF 1.9.0_rc1 is intended to be built
provides builtin rules to load external HTTP source packages.

New versions of Bazel remove these builtins and offer equivalent
functionality through standard library functions.

This patch replaces the builtin rules with their corresponding library
calls, to allow building of TF 1.9.0_rc1 with new versions of Bazel.

This is also done to one external dependency by modifying its URL / hash
to reference a patched version in the the Chrome OS distfiles
repository.

Longer-term, this problem will be solved by uprevving the TF package.

Author: martis@chromium.org
---
 WORKSPACE                | 17 +++++++++--------
 tensorflow/workspace.bzl |  4 +++-
 2 files changed, 12 insertions(+), 9 deletions(-)

--- a/WORKSPACE
+++ b/WORKSPACE
@@ -1,12 +1,13 @@
 workspace(name = "org_tensorflow")
 
+load("@bazel_tools//tools/build_defs/repo:http.bzl", "http_archive")
+
 http_archive(
     name = "io_bazel_rules_closure",
-    sha256 = "a38539c5b5c358548e75b44141b4ab637bba7c4dc02b46b1f62a96d6433f56ae",
+    sha256 = "ae5a3c3afb900bad8ab626b1a4c2dd78339862c373a3122afdd475cfab400e53",
     strip_prefix = "rules_closure-dbb96841cc0a5fb2664c37822803b06dab20c7d1",
     urls = [
-        "https://mirror.bazel.build/github.com/bazelbuild/rules_closure/archive/dbb96841cc0a5fb2664c37822803b06dab20c7d1.tar.gz",
-        "https://github.com/bazelbuild/rules_closure/archive/dbb96841cc0a5fb2664c37822803b06dab20c7d1.tar.gz",  # 2018-04-13
+        "http://commondatastorage.googleapis.com/chromeos-localmirror/distfiles/tensorflow-1.9.0_rc1-rules_closure-patched.tar.gz",
     ],
 )
 
@@ -46,7 +47,7 @@ load("//tensorflow:workspace.bzl", "tf_workspace")
 # Please add all new TensorFlow dependencies in workspace.bzl.
 tf_workspace()
 
-new_http_archive(
+http_archive(
     name = "inception_v1",
     build_file = "models.BUILD",
     sha256 = "7efe12a8363f09bc24d7b7a450304a15655a57a7751929b2c1593a71183bb105",
@@ -56,7 +57,7 @@ new_http_archive(
     ],
 )
 
-new_http_archive(
+http_archive(
     name = "mobile_ssd",
     build_file = "models.BUILD",
     sha256 = "bddd81ea5c80a97adfac1c9f770e6f55cbafd7cce4d3bbe15fbeb041e6b8f3e8",
@@ -66,7 +67,7 @@ new_http_archive(
     ],
 )
 
-new_http_archive(
+http_archive(
     name = "mobile_multibox",
     build_file = "models.BUILD",
     sha256 = "859edcddf84dddb974c36c36cfc1f74555148e9c9213dedacf1d6b613ad52b96",
@@ -76,7 +77,7 @@ new_http_archive(
     ],
 )
 
-new_http_archive(
+http_archive(
     name = "stylize",
     build_file = "models.BUILD",
     sha256 = "3d374a730aef330424a356a8d4f04d8a54277c425e274ecb7d9c83aa912c6bfa",
@@ -86,7 +87,7 @@ new_http_archive(
     ],
 )
 
-new_http_archive(
+http_archive(
     name = "speech_commands",
     build_file = "models.BUILD",
     sha256 = "c3ec4fea3158eb111f1d932336351edfe8bd515bb6e87aad4f25dbad0a600d0c",
--- a/tensorflow/workspace.bzl
+++ b/tensorflow/workspace.bzl
@@ -1,5 +1,7 @@
 # TensorFlow external dependencies that can be loaded in WORKSPACE files.
 
+load("@bazel_tools//tools/build_defs/repo:http.bzl", "http_archive")
+
 load("//third_party/gpus:cuda_configure.bzl", "cuda_configure")
 load("//third_party/tensorrt:tensorrt_configure.bzl", "tensorrt_configure")
 load("//third_party:nccl/nccl_configure.bzl", "nccl_configure")
@@ -694,7 +696,7 @@ def tf_workspace(path_prefix="", tf_repo_name=""):
       build_file = clean_dep("//third_party/flatbuffers:flatbuffers.BUILD"),
   )
 
-  native.new_http_archive(
+  http_archive(
       name = "double_conversion",
       urls = [
           "https://github.com/google/double-conversion/archive/3992066a95b823efc8ccc1baf82a1cfc73f6e9b8.zip",
-- 
2.21.0.593.g511ec345e18-goog

