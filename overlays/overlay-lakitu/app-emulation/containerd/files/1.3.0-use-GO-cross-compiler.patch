From 51b740efb7803b51be0b62711fc8109c7c52144a Mon Sep 17 00:00:00 2001
From: Dexter Rivera <riverade@google.com>
Date: Wed, 9 Oct 2019 00:55:07 +0000
Subject: [PATCH] 1.3.0-use-GO-cross-compiler

---
 Makefile | 42 +++++++++++++++++++++---------------------
 1 file changed, 21 insertions(+), 21 deletions(-)

diff --git a/Makefile b/Makefile
index 67581618..2d20fbcf 100644
--- a/Makefile
+++ b/Makefile
@@ -24,9 +24,9 @@ VERSION=$(shell git describe --match 'v[0-9]*' --dirty='.m' --always)
 REVISION=$(shell git rev-parse HEAD)$(shell if ! git diff --no-ext-diff --quiet --exit-code; then echo .m; fi)
 PACKAGE=github.com/containerd/containerd
 
-ifneq "$(strip $(shell command -v go 2>/dev/null))" ""
-	GOOS ?= $(shell go env GOOS)
-	GOARCH ?= $(shell go env GOARCH)
+ifneq "$(strip $(shell command -v ${GO} 2>/dev/null))" ""
+	GOOS ?= $(shell ${GO} env GOOS)
+	GOARCH ?= $(shell ${GO} env GOARCH)
 else
 	ifeq ($(GOOS),)
 		# approximate GOOS for the platform if we don't have Go and GOOS isn't
@@ -68,7 +68,7 @@ RELEASE=containerd-$(VERSION:v%=%).${GOOS}-${GOARCH}
 PKG=github.com/containerd/containerd
 
 # Project packages.
-PACKAGES=$(shell go list ./... | grep -v /vendor/)
+PACKAGES=$(shell ${GO} list ./... | grep -v /vendor/)
 INTEGRATION_PACKAGE=${PKG}
 TEST_REQUIRES_ROOT_PACKAGES=$(filter \
     ${PACKAGES}, \
@@ -110,7 +110,7 @@ GO_GCFLAGS=$(shell				\
 
 BINARIES=$(addprefix bin/,$(COMMANDS))
 
-# Flags passed to `go test`
+# Flags passed to `${GO} test`
 TESTFLAGS ?= $(TESTFLAGS_RACE)
 TESTFLAGS_PARALLEL ?= 8
 
@@ -130,7 +130,7 @@ AUTHORS: .mailmap .git/HEAD
 
 generate: protos
 	@echo "$(WHALE) $@"
-	@PATH="${ROOTDIR}/bin:${PATH}" go generate -x ${PACKAGES}
+	@PATH="${ROOTDIR}/bin:${PATH}" ${GO} generate -x ${PACKAGES}
 
 protos: bin/protoc-gen-gogoctrd ## generate protobuf
 	@echo "$(WHALE) $@"
@@ -155,44 +155,44 @@ proto-fmt: ## check format of proto files
 	@test -z "$$(find . -path ./vendor -prune -o -name '*.proto' -type f -exec grep -Hn "Meta meta = " {} \; | grep -v '(gogoproto.nullable) = false' | tee /dev/stderr)" || \
 		(echo "$(ONI) meta fields in proto files must have option (gogoproto.nullable) = false" && false)
 
-build: ## build the go packages
+build: ## build the ${GO} packages
 	@echo "$(WHALE) $@"
-	@go build ${DEBUG_GO_GCFLAGS} ${GO_GCFLAGS} ${GO_BUILD_FLAGS} ${EXTRA_FLAGS} ${GO_LDFLAGS} ${PACKAGES}
+	@${GO} build ${DEBUG_GO_GCFLAGS} ${GO_GCFLAGS} ${GO_BUILD_FLAGS} ${EXTRA_FLAGS} ${GO_LDFLAGS} ${PACKAGES}
 
 test: ## run tests, except integration tests and tests that require root
 	@echo "$(WHALE) $@"
-	@go test ${TESTFLAGS} $(filter-out ${INTEGRATION_PACKAGE},${PACKAGES})
+	@${GO} test ${TESTFLAGS} $(filter-out ${INTEGRATION_PACKAGE},${PACKAGES})
 
 root-test: ## run tests, except integration tests
 	@echo "$(WHALE) $@"
-	@go test ${TESTFLAGS} $(filter-out ${INTEGRATION_PACKAGE},${TEST_REQUIRES_ROOT_PACKAGES}) -test.root
+	@${GO} test ${TESTFLAGS} $(filter-out ${INTEGRATION_PACKAGE},${TEST_REQUIRES_ROOT_PACKAGES}) -test.root
 
 integration: ## run integration tests
 	@echo "$(WHALE) $@"
-	@go test ${TESTFLAGS} -test.root -parallel ${TESTFLAGS_PARALLEL}
+	@${GO} test ${TESTFLAGS} -test.root -parallel ${TESTFLAGS_PARALLEL}
 
 benchmark: ## run benchmarks tests
 	@echo "$(WHALE) $@"
-	@go test ${TESTFLAGS} -bench . -run Benchmark -test.root
+	@${GO} test ${TESTFLAGS} -bench . -run Benchmark -test.root
 
 FORCE:
 
 # Build a binary from a cmd.
 bin/%: cmd/% FORCE
 	@echo "$(WHALE) $@${BINARY_SUFFIX}"
-	@go build ${DEBUG_GO_GCFLAGS} ${GO_GCFLAGS} ${GO_BUILD_FLAGS} -o $@${BINARY_SUFFIX} ${GO_LDFLAGS} ${GO_TAGS}  ./$<
+	@${GO} build ${DEBUG_GO_GCFLAGS} ${GO_GCFLAGS} ${GO_BUILD_FLAGS} -o $@${BINARY_SUFFIX} ${GO_LDFLAGS} ${GO_TAGS}  ./$<
 
 bin/containerd-shim: cmd/containerd-shim FORCE # set !cgo and omit pie for a static shim build: https://github.com/golang/go/issues/17789#issuecomment-258542220
 	@echo "$(WHALE) bin/containerd-shim"
-	@CGO_ENABLED=0 go build ${GO_BUILD_FLAGS} -o bin/containerd-shim ${SHIM_GO_LDFLAGS} ${GO_TAGS} ./cmd/containerd-shim
+	@CGO_ENABLED=0 ${GO} build ${GO_BUILD_FLAGS} -o bin/containerd-shim ${SHIM_GO_LDFLAGS} ${GO_TAGS} ./cmd/containerd-shim
 
 bin/containerd-shim-runc-v1: cmd/containerd-shim-runc-v1 FORCE # set !cgo and omit pie for a static shim build: https://github.com/golang/go/issues/17789#issuecomment-258542220
 	@echo "$(WHALE) bin/containerd-shim-runc-v1"
-	@CGO_ENABLED=0 go build ${GO_BUILD_FLAGS} -o bin/containerd-shim-runc-v1 ${SHIM_GO_LDFLAGS} ${GO_TAGS} ./cmd/containerd-shim-runc-v1
+	@CGO_ENABLED=0 ${GO} build ${GO_BUILD_FLAGS} -o bin/containerd-shim-runc-v1 ${SHIM_GO_LDFLAGS} ${GO_TAGS} ./cmd/containerd-shim-runc-v1
 
 bin/containerd-shim-runc-v2: cmd/containerd-shim-runc-v2 FORCE # set !cgo and omit pie for a static shim build: https://github.com/golang/go/issues/17789#issuecomment-258542220
 	@echo "$(WHALE) bin/containerd-shim-runc-v2"
-	@CGO_ENABLED=0 go build ${GO_BUILD_FLAGS} -o bin/containerd-shim-runc-v2 ${SHIM_GO_LDFLAGS} ${GO_TAGS} ./cmd/containerd-shim-runc-v2
+	@CGO_ENABLED=0 ${GO} build ${GO_BUILD_FLAGS} -o bin/containerd-shim-runc-v2 ${SHIM_GO_LDFLAGS} ${GO_TAGS} ./cmd/containerd-shim-runc-v2
 
 binaries: $(BINARIES) ## build binaries
 	@echo "$(WHALE) $@"
@@ -204,7 +204,7 @@ mandir:
 	@mkdir -p man
 
 genman: FORCE
-	go run cmd/gen-manpages/main.go man/
+	${GO} run cmd/gen-manpages/main.go man/
 
 man/%: docs/man/%.md FORCE
 	@echo "$(WHALE) $<"
@@ -266,9 +266,9 @@ uninstall:
 coverage: ## generate coverprofiles from the unit tests, except tests that require root
 	@echo "$(WHALE) $@"
 	@rm -f coverage.txt
-	@go test -i ${TESTFLAGS} $(filter-out ${INTEGRATION_PACKAGE},${PACKAGES}) 2> /dev/null
+	@${GO} test -i ${TESTFLAGS} $(filter-out ${INTEGRATION_PACKAGE},${PACKAGES}) 2> /dev/null
 	@( for pkg in $(filter-out ${INTEGRATION_PACKAGE},${PACKAGES}); do \
-		go test ${TESTFLAGS} \
+		${GO} test ${TESTFLAGS} \
 			-cover \
 			-coverprofile=profile.out \
 			-covermode=atomic $$pkg || exit; \
@@ -280,9 +280,9 @@ coverage: ## generate coverprofiles from the unit tests, except tests that requi
 
 root-coverage: ## generate coverage profiles for unit tests that require root
 	@echo "$(WHALE) $@"
-	@go test -i ${TESTFLAGS} $(filter-out ${INTEGRATION_PACKAGE},${TEST_REQUIRES_ROOT_PACKAGES}) 2> /dev/null
+	@${GO} test -i ${TESTFLAGS} $(filter-out ${INTEGRATION_PACKAGE},${TEST_REQUIRES_ROOT_PACKAGES}) 2> /dev/null
 	@( for pkg in $(filter-out ${INTEGRATION_PACKAGE},${TEST_REQUIRES_ROOT_PACKAGES}); do \
-		go test ${TESTFLAGS} \
+		${GO} test ${TESTFLAGS} \
 			-cover \
 			-coverprofile=profile.out \
 			-covermode=atomic $$pkg -test.root || exit; \
-- 
2.23.0.581.g78d2f28ef7-goog

