ó
Ûë+]c           @   sL   d  Z  d e f d     YZ d e f d     YZ d e f d     YZ d S(   sC   Classes representing the monitoring interface for tasks or devices.t   Targetc           B   sD   e  Z d  Z d   Z d   Z d   Z d   Z d   Z d   Z RS(   sÍ  Abstract base class for a monitoring target.

  A Target is a "thing" that should be monitored, for example, a device or a
  process. The majority of the time, a single process will have only a single
  Target.

  Do not directly instantiate an object of this class.
  Use the concrete child classes instead:
  * TaskTarget to monitor a job or tasks running in (potentially) many places;
  * DeviceTarget to monitor a host machine that may be running a task.
  c         C   s   t    |  _ d  S(   N(   t   tuplet   _fields(   t   self(    (    sO   /home/dev01/chromiumos/chromite/third_party/infra_libs/ts_mon/common/targets.pyt   __init__   s    c         C   s   t     d S(   s/   Populate the 'target' into a MetricsCollection.N(   t   NotImplementedError(   R   t   collection_pb(    (    sO   /home/dev01/chromiumos/chromite/third_party/infra_libs/ts_mon/common/targets.pyt   populate_target_pb   s    c            s     f d     j  D S(   s+   Return target field values as a dictionary.c            s"   i  |  ] } t    |  |  q S(    (   t   getattr(   t   .0t   field(   R   (    sO   /home/dev01/chromiumos/chromite/third_party/infra_libs/ts_mon/common/targets.pys
   <dictcomp>   s   	 (   R   (   R   (    (   R   sO   /home/dev01/chromiumos/chromite/third_party/infra_libs/ts_mon/common/targets.pyt   to_dict   s    c         C   s`   xY | j    D]K \ } } | |  j k r; t d |   n  t |  |  t |  | |  q Wd S(   s4   Update values of some target fields given as a dict.s   Bad target field: %sN(   t	   iteritemsR   t   AttributeErrorR   t   setattr(   R   t   target_fieldsR
   t   value(    (    sO   /home/dev01/chromiumos/chromite/third_party/infra_libs/ts_mon/common/targets.pyt   update!   s
    c         C   sV   t  |   t  |  k r t Sx3 |  j D]( } t |  |  t | |  k r& t Sq& Wt S(   N(   t   typet   FalseR   R   t   True(   R   t   otherR
   (    (    sO   /home/dev01/chromiumos/chromite/third_party/infra_libs/ts_mon/common/targets.pyt   __eq__*   s    c         C   s   t  t t |  j      S(   N(   t   hashR   t   sortedR   (   R   (    (    sO   /home/dev01/chromiumos/chromite/third_party/infra_libs/ts_mon/common/targets.pyt   __hash__4   s    (	   t   __name__t
   __module__t   __doc__R   R   R   R   R   R   (    (    (    sO   /home/dev01/chromiumos/chromite/third_party/infra_libs/ts_mon/common/targets.pyR       s   						
t   DeviceTargetc           B   s    e  Z d  Z d   Z d   Z RS(   sD   Monitoring interface class for monitoring specific hosts or devices.c         C   sV   t  t |   j   | |  _ | |  _ | |  _ | |  _ d |  _ t |  _	 d |  _
 d S(   s=  Create a Target object exporting info about a specific device.

    Args:
      region (str): physical region in which the device is located.
      role (str): role of the device.
      network (str): virtual network on which the device is located.
      hostname (str): name by which the device self-identifies.
    t
   ACQ_CHROMEt   regiont   rolet   networkt   hostnameN(   s   regions   roles   networks   hostname(   t   superR   R   R   R    R!   R"   t   realmR   t	   alertableR   (   R   R   R    R!   R"   (    (    sO   /home/dev01/chromiumos/chromite/third_party/infra_libs/ts_mon/common/targets.pyR   :   s    							c         C   s^   |  j  | j _ |  j | j _ |  j | j _ |  j | j _ |  j | j _ |  j | j _ d S(   sº   Populate the 'network_device' target into metrics_pb2.MetricsCollection.

    Args:
      collection (metrics_pb2.MetricsCollection): the collection proto to be
          populated.
    N(	   R   t   network_devicet   metroR    R!   t	   hostgroupR"   R$   R%   (   R   t
   collection(    (    sO   /home/dev01/chromiumos/chromite/third_party/infra_libs/ts_mon/common/targets.pyR   L   s    (   R   R   R   R   R   (    (    (    sO   /home/dev01/chromiumos/chromite/third_party/infra_libs/ts_mon/common/targets.pyR   7   s   	t
   TaskTargetc           B   s#   e  Z d  Z d d  Z d   Z RS(   sC   Monitoring interface class for monitoring active jobs or processes.i    c         C   sM   t  t |   j   | |  _ | |  _ | |  _ | |  _ | |  _ d |  _ d S(   sy  Create a Target object exporting info about a specific task.

    Args:
      service_name (str): service of which this task is a part.
      job_name (str): specific name of this task.
      region (str): general region in which this task is running.
      hostname (str): specific machine on which this task is running.
      task_num (int): replication id of this task.
    t   service_namet   job_nameR   R"   t   task_numN(   s   service_names   job_names   regions   hostnames   task_num(	   R#   R*   R   R+   R,   R   R"   R-   R   (   R   R+   R,   R   R"   R-   (    (    sO   /home/dev01/chromiumos/chromite/third_party/infra_libs/ts_mon/common/targets.pyR   ^   s    
					 c         C   sO   |  j  | j _  |  j | j _ |  j | j _ |  j | j _ |  j | j _ d S(   s°   Populate the 'task' target into metrics_pb2.MetricsCollection.

    Args:
      collection (metrics_pb2.MetricsCollection): the collection proto to be
          populated.
    N(   R+   t   taskR,   R   t   data_centerR"   t	   host_nameR-   (   R   R)   (    (    sO   /home/dev01/chromiumos/chromite/third_party/infra_libs/ts_mon/common/targets.pyR   q   s
    (   R   R   R   R   R   (    (    (    sO   /home/dev01/chromiumos/chromite/third_party/infra_libs/ts_mon/common/targets.pyR*   [   s   N(   R   t   objectR    R   R*   (    (    (    sO   /home/dev01/chromiumos/chromite/third_party/infra_libs/ts_mon/common/targets.pyt   <module>   s   /$