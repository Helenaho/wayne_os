// Copyright 2019 The Chromium OS Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

syntax = "proto3";

package test_platform.steps;

import "test_platform/request.proto";
import "test_platform/steps/enumeration.proto";
import "test_platform/config/config.proto";
import "test_platform/taskstate.proto";

option go_package = "go.chromium.org/chromiumos/infra/proto/go/test_platform/steps";

// ExecuteRequest defines the input of the Execute step.
message ExecuteRequest {
    // RequestParams are the request-wide parameters for the
    // cros_test_platform request. This includes all the request-wide information
    // needed in order to execute tests (e.g. labels, retry parameters,
    // scheduling parameters).
    test_platform.Request.Params request_params = 1;

    EnumerationResponse enumeration = 2;

    test_platform.config.Config config = 3;
}

// ExecuteResponse defines the output of the Execute step.
message ExecuteResponse {
    message TaskResult {
        reserved 1;
        reserved "task_id";

        // URL at which a human can view the task's run state (e.g. link to
        // swarming or cautotest job view).
        string task_url = 2;

        TaskState state = 3;

        string name = 4;

        // URL at which a human can view the task's logs (e.g. a link to
        // stainless gs viewer for this task).
        string log_url = 5;
    }

    repeated TaskResult task_results = 1;

    // State encodes the state of the overall execution, including the ultimate
    // verdict (taking into account any sub-task verdicts).
    TaskState state = 2;
}
