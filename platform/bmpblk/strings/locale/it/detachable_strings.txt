Opzioni sviluppatore
Mostra informazioni di debug
Attiva la verifica del sistema operativo
Spegni
Lingua
Avvia dalla rete
Avvia Legacy BIOS
Avvia da USB
Avvia da USB o scheda SD
Avvia da disco interno
Annulla
Conferma l'attivazione della verifica del sistema operativo
Disattiva la verifica del sistema operativo
Conferma la disattivazione della verifica del sistema operativo
Utilizza i pulsanti del volume per spostarti verso l'alto o verso il basso
e il tasto di accensione per selezionare un'opzione.
La disattivazione della verifica del sistema operativo rende il sistema NON SICURO.
Seleziona "Annulla" continuare a usufruire della protezione.
La verifica del sistema operativo è NON È ATTIVA. Il sistema risulta NON SICURO.
Seleziona "Attiva la verifica del sistema operativo" per ripristinare la protezione.
Seleziona "Conferma l'attivazione della verifica del sistema operativo" per proteggere il sistema.
