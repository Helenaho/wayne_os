Entwickleroptionen
Informationen zur Fehlerbehebung anzeigen
Betriebssystemüberprüfung aktivieren
Ausschalten
Sprache
Vom Netzwerk starten
Legacy BIOS starten
Von USB starten
Von USB oder SD-Karte starten
Vom internen Laufwerk starten
Abbrechen
Aktivierung der Betriebssystemüberprüfung bestätigen
Betriebssystemüberprüfung deaktivieren
Deaktivierung der Betriebssystemüberprüfung bestätigen
Verwenden Sie die Lautstärketasten, um nach oben oder nach unten zu navigieren.
Über die Ein-/Aus-Taste können Sie die gewünschte Option auswählen.
Durch Deaktivieren der Betriebssystemüberprüfung wird Ihr System UNSICHER.
Wählen Sie "Abbrechen" aus, damit Ihr System weiterhin geschützt bleibt.
Die Betriebssystemüberprüfung ist AUS. Ihr System ist UNSICHER.
Wählen Sie "Betriebssystemüberprüfung aktivieren" aus, um Ihr System wieder zu schützen.
Wählen Sie "Aktivierung der Betriebssystemüberprüfung bestätigen" aus, um Ihr System zu schützen.
