# Copyright 2015 The Chromium OS Authors. All rights reserved.
# Use of this source code is governed by a BSD-style license that can be
# found in the LICENSE file.

from distutils.core import setup


setup(name='webplot',
      version='1.0',
      description='Web drawing tool for touch devices',
      long_description='It is a tool displaying finger traces on a browser. '
                       'It consists of a cherrypy server and browser clients.',
      license='BSD-Google',
      packages=['webplot',
                'webplot.remote',
                'webplot.remote.mt',
                'webplot.remote.mt.input'],
      package_data={'webplot': ['*.html', '*.js', 'webplot'],
                    'webplot.remote': ['data/*',]},
      author='Joseph Hwang',
      author_email='josephsih@chromium.org',
)
